..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit vmod.vcc and run make instead
..

.. role:: ref(emphasis)

.. _vmod_health(3):

===========
vmod_health
===========

*WARNING: as of May 2018, this vmod does not work with varnish-cache
master due to changes in the backend/director API*

----------------------
backend health details
----------------------

:Manual section: 3

SYNOPSIS
========

import health [from "path"] ;


::

  BOOL health.has_probe(BACKEND)
  BOOL health.probe_healthy(BACKEND)
  STRING health.admin_health(BACKEND)
  TIME health.health_changed(BACKEND)

  STRING version()

DESCRIPTION
===========

This Varnish Module (VMOD) makes details about the health of a backend
available in VCL -- the information shown in the output of the
``backend.list`` command of the Varnish CLI, which is commonly invoked
via ``varnishadm``.

If a health probe has been defined for a backend, then its health or
sickness may be determined by the results of the probe. But if the
``backend.set_health`` command has been invoked to set the backend
manually to healthy or sick, then that health state will apply in the
decisions that Varnish makes to choose backends. The ``std.healthy()``
function from the ``std`` VMOD returns the result of that decision,
but does not distinguish whether it resulted from a probe or from the
use of ``backend.set_health``.

The functions in this VMOD make that distinction possible in VCL -- it
can be determined, for example, that a backend is healthy according to
its probe, but has been set to sick via the CLI. The states of
administrative health are the same as those seen in the output of
``backend.list`` command: ``"probe"``, ``"healthy"`` or ``"sick"`` if
the backend health is determined by the probe, manually set to
healthy, or manually set to unhealthy, respectively.

Examples::

  sub vcl_recv {

      # If backend b has a health probe, log the state of the probe.
      if (health.has_probe(b)) {
          if (health.probe_healthy(b)) {
              std.log("Backend b probe result is good");
          }
          else {
              std.log("Backend b probe result is bad");
          }
      }

      # Log whether the health of backend b was manually set.
      if (health.admin_health(b) == "sick") {
          std.log("Backend b manually set to unhealthy");
      }
      elsif (health.admin_health(b) == "healthy") {
          std.log("Backend b manually set to healthy");
      }
      else {
          # admin_health() returns "probe"
          std.log("Backend b health not manually set");
      }

      std.log("Backend b health last changed at "
              + health.health_changed(b));
  }

CONTENTS
========

* BOOL has_probe(BACKEND)
* BOOL probe_healthy(BACKEND)
* STRING admin_health(BACKEND)
* TIME health_changed(BACKEND)
* STRING version()

.. _func_has_probe:

has_probe
---------

::

	BOOL has_probe(BACKEND)

Return true if a probe has been defined for the backend. If no probe has
been defined, then the ``probe_healthy`` function below cannot be used,
so this function makes it possible to check for that.

Example::

  backend b { .host = "1.2.3.4";
              .probe = { .url = "/"; .interval = 1s; timeout = 1s; }
            }

  sub vcl_recv {
      if (health.has_probe(b)) {
          call do_if_a_probe_exists;
      }
  }

.. _func_probe_healthy:

probe_healthy
-------------

::

	BOOL probe_healthy(BACKEND)

Return true if the backend is healthy according to the results of its
probe, independent of any state set manually by
``backend.set_health``.

This function MAY NOT be invoked for a backend unless a probe is
assigned to it, which is not necessarily the case for every
backend. If it is called for a backend without a probe, then VCL
failure is invoked: response status 503 is returned, and an error
message with the tag ``VCL_Error`` is emitted to the Varnish log.

To avoid that, call ``has_probe()`` first, and call this function only
if ``has_probe()`` returns true.

Example::

  sub vcl_recv {
      if (health.has_probe(b)) {
          if (health.probe_healthy(b)) {
              return(synth(200, "Health checks for b are good"));
          }
          else {
              return(synth(503, "Health checks for b are failing"));
          }
      }
  }

.. _func_admin_health:

admin_health
------------

::

	STRING admin_health(BACKEND)

Return a string indicating the administrative state of the backend's health.
These are the same strings displayed in the output of the ``backend.list``
command:

* ``"healthy"``: backend set to healthy via ``backend.set_health``,
  regardless of the results of any probe

* ``"sick"``: backend set to sick regardless of any probe

* ``"probe"``: backend health is not manually set, and hence is either
  determined by its probe, or there is no probe

* ``"deleted"``: the backend has been deleted and will be removed

The ``"deleted"`` state is rarely seen in the output of
``backend.list``, but it is possible, for example, if the backend has
been marked for deletion by the action of a VMOD but has not yet been
fully de-allocated.

If no probe has been defined for a backend, and its health has not
been manually set, then the result is ``"probe"``. This may seem
contradictory, but it simply means that neither the ``"healthy"`` nor
the ``"sick"`` states apply, which would indicate that health was
manually set. The backend is always regarded as healthy in this
situation -- that is, requests are always sent to it, since no health
probe and no manual health state indicate that they should not be.

Example::

  sub vcl_recv {
      if (health.admin_health(b) == "sick") {
          return(synth(503, "Backend b manually set to sick"));
      }
  }

.. _func_health_changed:

health_changed
--------------

::

	TIME health_changed(BACKEND)

Return the time at which the health state of the backend last changed.

Example::

  sub vcl_recv {
      if (health.has_probe(b)) {
          if (health.probe_healthy(b)) {
              std.log("Backend b healthy since: "
                      + health.health_changed(b));
          }
      }
  }
  

.. _func_version:

version
-------

::

	STRING version()

Return the version string for this VMOD.

Example::

  std.log("Using VMOD health version: " + health.version());

REQUIREMENTS
============

This VMOD has been tested with Varnish version 5.1.2.

INSTALLATION
============

See `INSTALL.rst <INSTALL.rst>`_ in the source repository.

SEE ALSO
========

* varnishd(1)
* vcl(7)
* varnish-cli(7)
* varnishadm(1)
* vmod_std(3)
* VMOD source repository: https://code.uplex.de/uplex-varnish/libvmod-health

COPYRIGHT
=========

::

  This document is licensed under the same conditions
  as the libvmod-health project. See LICENSE for details.
 
  Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 
