/*-
 * Copyright 2017 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"

#include "cache/cache.h"
#include "cache/cache_director.h"
#include "cache/cache_backend.h"

#include "vcc_if.h"

#define FAIL(ctx, fmt, ...) \
	VRT_fail((ctx), "vmod health error: " fmt, __VA_ARGS__)

#define NOTABACKEND(ctx, d) \
	FAIL((ctx), "%s is a director, not a backend", (d)->vcl_name)

VCL_BOOL
vmod_has_probe(VRT_CTX, VCL_BACKEND d)
{
	struct backend *b;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(d, DIRECTOR_MAGIC);
	if (d->priv == NULL) {
		NOTABACKEND(ctx, d);
		return 0;
	}

	CAST_OBJ(b, d->priv, BACKEND_MAGIC);
	return (b->probe != NULL);
}

VCL_BOOL
vmod_probe_healthy(VRT_CTX, VCL_BACKEND d)
{
	struct backend *b;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(d, DIRECTOR_MAGIC);
	if (d->priv == NULL) {
		NOTABACKEND(ctx, d);
		return 0;
	}

	CAST_OBJ(b, d->priv, BACKEND_MAGIC);
	if (b->probe == NULL) {
		FAIL(ctx, "Backend %s has no health probe", d->vcl_name);
		return (0);
	}
	return (d->health);
}

VCL_STRING
vmod_admin_health(VRT_CTX, VCL_BACKEND d)
{
	(void) ctx;

	return (VDI_Ahealth(d));
}

VCL_TIME
vmod_health_changed(VRT_CTX, VCL_BACKEND d)
{
	double changed;
	(void) ctx;

	VDI_Healthy(d, &changed);
	return (changed);
}

VCL_STRING
vmod_version(VRT_CTX)
{
	(void) ctx;
	return VERSION;
}
